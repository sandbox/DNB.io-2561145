
Dyslexia
=============

Introduction
------------

Provides a quick solution to allow visitors to switch between the active
font family and a specific one adapted to people with reading disorders.


Installation and setup
----------------------

Copy the entire module directory under sites/all/modules (typically),
just like any other Drupal modules.

For the module to work, you will need to install the "regular" set
of the OpenDyslexic 3 font. The module awaits the truetype file to
be placed under sites/all/fonts/OpenDyslexic3-Regular.ttf. 

You can find the download link on https://opendyslexic.org
(Donations for the hard work must surely be much appreciated).

Next, you will find the module settings page under :
Configuration > User interface > Dyslexia

Once set, you can trigger the dyslexia function in two ways :
-on any page, hit the tab key to "Toggle Dyslexia" and press enter ;
-or use the block that you will find in your theme blocks list. 


About the "Dyslexia" module
--------------------------------

Module by DNB.io based on the High Contrast module by hkirsman.
https://www.drupal.org/project/high_contrast
https://www.drupal.org/u/hkirsman

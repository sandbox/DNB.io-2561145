/**
 * @file
 * Integrates the dyslexia toggle in the skip-link container.
 */

(function ($) {
  'use strict';
  var $skipLink;
  $skipLink = $('#skip-link');
  if ($skipLink.length) {
    $skipLink.append(Drupal.settings.dyslexia.link);
  }
})
(jQuery);
